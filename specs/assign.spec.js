describe("Cloning an object", () => {
	const input = { a: 1 };

	const output = { a: 1 };

	test("Cloning an object", () => {
		expect(Object.assign({}, input)).toEqual(output);
	});

});
  
describe("Merging objects", () => {
	const input1 = { a: 1 };
	const input2 = { b: 2 };
	const input3 = { c: 3 };

	const output = { a: 1, b: 2, c: 3 };

	test("Merging objects", () => {
		expect(Object.assign(input1, input2, input3)).toEqual(output);
	});
	test("Overwriting target object", () => {
		expect(input1).toEqual(output);
	});

});
  
describe("Merging objects with same properties", () => {
	const input1 = { a: 1, b: 1, c: 1 };
	const input2 = { b: 2, c: 2 };
	const input3 = { c: 3 };

	const output = { a: 1, b: 2, c: 3 };

	test("Merging objects with same properties", () => {
		expect(Object.assign({}, input1, input2, input3)).toEqual(output);
	});
	test("Target object stays the same", () => {
		expect(input1).toEqual({ a: 1, b: 1, c: 1 });
	});

});
  
describe("Assign primitives", () => {
	const input1 = "abc";
	const input2 = true;
	const input3 = 10;
	const input4 = Symbol("foo");
  
	const output = { "0": "a", "1": "b", "2": "c" };
  
	test("Primitives will be wrapped to objects", () => {
		expect(Object.assign({}, input1, null, input2, undefined, input3, input4)).toEqual(output);
	});
  
});