let originalObject = {
	a: 1,
	b: "aa",
	c: {e: true, f: "123"}
};
  
describe("isFrozen() method", () => {
	const isFrozenObject = Object.freeze({}, Object.assign({}, originalObject));

	test("isFrozen() outputs", () => {
		expect(Object.isFrozen(originalObject)).toBe(false);
		expect(Object.isFrozen(isFrozenObject)).toBe(true);
	});

});

describe("isSealed() method", () => {
	const isSealedObject = Object.seal({}, Object.assign({}, originalObject));

	test("isSealed() outputs", () => {
		expect(Object.isSealed(originalObject)).toBe(false);
		expect(Object.isSealed(isSealedObject)).toBe(true);
	});

});
  
describe("isExtensible() method", () => {
	const isExtensibleObject = Object.preventExtensions({}, Object.assign({}, originalObject));

	test("isExtensible() outputs", () => {
		expect(Object.isSealed(originalObject)).toBe(false);
		expect(Object.isSealed(isExtensibleObject)).toBe(true);
	});

});