describe("defineProperty Writable attribute", () => {
	let object = {};

	Object.defineProperty(object, "property", {
		value: 42,
		writable: false
	});

	object.property = 77; // throws an error in strict mode

	test("property will not be changed if Writable attribute is false", () => {
		expect(object.property).toBe(42);
	});

});
  
describe("defineProperty Enumerable attribute", () => {
	let object = {};

	Object.defineProperty(object, "a", {
		value: 1,
		enumerable: true
	});

	Object.defineProperty(object, "b", {
		value: 2,
		enumerable: false
	});

	Object.defineProperty(object, "c", {
		value: 3
	}); // enumerable defaults to false

	object.d = 4; // enumerable defaults to true

	Object.defineProperty(object, Symbol.for("e"), {
		value: 5,
		enumerable: true
	});

	Object.defineProperty(object, Symbol.for("f"), {
		value: 6,
		enumerable: false
	});

	test("Enumerable properties", () => {
		expect(Object.keys(object)).toEqual(["a", "d"]);

		expect(object.propertyIsEnumerable("a")).toBe(true);
		expect(object.propertyIsEnumerable("b")).toBe(false);
		expect(object.propertyIsEnumerable("c")).toBe(false);
		expect(object.propertyIsEnumerable("d")).toBe(true);
		expect(object.propertyIsEnumerable(Symbol.for("e"))).toBe(true);
		expect(object.propertyIsEnumerable(Symbol.for("f"))).toBe(false);
	});
});
  
describe("defineProperty Configurable attribute", () => {
	let object = {};

	Object.defineProperty(object, "property", {
		get() { return 1; },
		configurable: false
	});

	delete object.property

	test("property will not be changed", () => {
		expect(object.property).toBe(1);
	});

});
  
describe("defineProperties() method", () => {
	let object = {};

	Object.defineProperties(object, {
		"property1": {
			value: 1,
			writable: true
		},
		"property2": {
			value: 2,
			writable: false
		}
	});

	object.property1 = 55;
	object.property2 = 55;

	test("defineProperties() method results", () => {
		expect(object.property1).toBe(55);
		expect(object.property2).toBe(2);
	});

});