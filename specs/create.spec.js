describe("create() method", () => {
	const person = {
		isHuman: false,
		printIntroduction: function() {
			return `My name is ${this.name}. Am I human? ${this.isHuman}`;
		}
	};

	const me = Object.create(person);

	me.name = "Nazar";
	me.isHuman = true;

	test("create() output", () => {
		expect(me.name).toBe("Nazar");
		expect(me.isHuman).toBe(true);
		expect(me.printIntroduction()).toBe("My name is Nazar. Am I human? true");
	});

});
  