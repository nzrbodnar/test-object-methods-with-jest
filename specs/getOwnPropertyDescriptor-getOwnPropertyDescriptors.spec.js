describe("getOwnPropertyDescriptor() method", () => {
	const object1 = { foo: 42 };
	const descriptor1 = Object.getOwnPropertyDescriptor(object1, "foo");

	const output1 = [["value", 42], ["writable", true], ["enumerable", true], ["configurable", true]]

	test("getOwnPropertyDescriptor() method output", () => {
		expect(Object.entries(descriptor1)).toEqual(output1);
	});

	const object2 = {};
	Object.defineProperty(object2, "foo", {
		value: 42
	});
	const descriptor2 = Object.getOwnPropertyDescriptor(object2, "foo");

	const output2 = [["value", 42], ["writable", false], ["enumerable", false], ["configurable", false]]

	test("getOwnPropertyDescriptor() method output", () => {
		expect(Object.entries(descriptor2)).toEqual(output2);
	});
});
  
describe("getOwnPropertyDescriptors() method", () => {
	const object = { p1: 42 };
	Object.defineProperty(object, "p2", {
		value: "123"
	});
	const descriptor = Object.getOwnPropertyDescriptors(object);

	const output = [ 
		[ 
			"p1",
			{ 
				value: 42,
				writable: true,
				enumerable: true,
				configurable: true 
			} 
		],
		[ 
			"p2",
			{ 
				value: "123",
				writable: false,
				enumerable: false,
				configurable: false 
			} 
		] 
	]

	test("getOwnPropertyDescriptors() method output", () => {
		expect(Object.entries(descriptor)).toEqual(output);
	});

});