describe("is() method", () => {
	const foo = { a: 1 };
	const bar = { a: 1 };

	test("is() method outputs", () => {
		expect(Object.is("foo", "foo")).toEqual(true);
		expect(Object.is(window, window)).toEqual(true);

		expect(Object.is("foo", "bar")).toEqual(false);
		expect(Object.is([], [])).toEqual(false);

		expect(Object.is(foo, foo)).toEqual(true);
		expect(Object.is(foo, bar)).toEqual(false);

		expect(Object.is(null, null)).toEqual(true);

		expect(Object.is(0, -0)).toEqual(false);
		expect(Object.is(-0, -0)).toEqual(true);
		expect(Object.is(NaN, 0/0)).toEqual(true);
	});
});