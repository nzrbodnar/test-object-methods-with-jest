describe("entries() method", () => {
	const input = { a: 1, b: "test" };

	const output = [["a", 1], ["b", "test"]];

	test("entries() method result", () => {
		expect(Object.entries(input)).toEqual(output);
	});

});