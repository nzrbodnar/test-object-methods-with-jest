function modifiedObj(obj) {
	obj.property = 333;
	obj.a = 222;
	delete obj.b;
	obj.c.e = false;
	delete obj.c.f;

	return obj;
}
  
let originalObject = {
	a: 1,
	b: "aa",
	c: {e: true, f: "123"}
};

describe("freeze() method", () => {
	let freezedObject = Object.freeze(Object.assign({}, originalObject));

	const freezedObjectOutput = {
		a: 1,
		b: "aa",
		c: {e: false}
	};

	test("not allowed any modifications (doesn't apply for child objects)", () => {
		expect(modifiedObj(freezedObject)).toEqual(freezedObjectOutput);
	});

});
  
describe("seal() method", () => {
	let sealedObject = Object.seal(Object.assign({}, originalObject));

	const sealedObjectOutput = {
		a: 222,
		b: "aa",
		c: {e: false}
	};

	test("method preventing new properties from being added to it and marking all existing properties as non-configurable", () => {
		expect(modifiedObj(sealedObject)).toEqual(sealedObjectOutput);
	});

});
  
describe("preventExtensions() method", () => {
	let preventExtensionsObject = Object.preventExtensions(Object.assign({}, originalObject));

	const preventExtensionsObjectOutput = {
		a: 222,
		c: {e: false}
	};

	test("method prevents new properties from ever being added to an object", () => {
		expect(modifiedObj(preventExtensionsObject)).toEqual(preventExtensionsObjectOutput);
	});

});