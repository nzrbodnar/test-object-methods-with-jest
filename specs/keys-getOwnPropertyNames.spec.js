let object = {};

Object.defineProperties(object, {
	one: {enumerable: true, value: 1},
	two: {enumerable: false, value: 2}
});

describe("keys() method", () => {
	test("keys() method output", () => {
		expect(Object.keys(object)).toEqual(["one"]);
	});
});

describe("getOwnPropertyNames() method", () => {
	test("getOwnPropertyNames() method output", () => {
		expect(Object.getOwnPropertyNames(object)).toEqual(["one", "two"]);
	});
});