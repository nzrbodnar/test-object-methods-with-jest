describe("getPrototypeOf() method", () => {
	const prototype = {};
	const object = Object.create(prototype);

	test("getPrototypeOf() method outputs", () => {
		expect(Object.getPrototypeOf(object) === prototype).toEqual(true);
	});
});

describe("setPrototypeOf() method", () => {
	const Animal = {}
	Animal.isAnimal = true

	const Mammal = Object.create(Animal)
	Mammal.isMammal = true

	const dog = Object.create(Animal)

	Object.setPrototypeOf(dog, Mammal)

	test("setPrototypeOf() method outputs", () => {
		expect(dog.isAnimal).toEqual(true);
		expect(dog.isMammal).toEqual(true);
	});
});